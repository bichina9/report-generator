Installation:
1. Install all things! `npm install && cd front && npm install`

Usage:

1. Edit table `sheet.xlsx`, modify Requirements as per new project version
2. Save table with the same filename and path. **IMPORTANT** - use `xlxs` extension
3. Run `node parse-xlsx.js`. This will create an `output.json` file in the project root folder, based on the `sheet.xlsx` contents
4. Run `npm start` to start server
5. Go to `localhost:3000`. On the left panel you will see a list of numbers, which correspond to Req IDS.
Each Req ID requires a separate document. 
Click on any Req id and there will appear a form in which you can modify predefined values from XLSX sheet
Each field can be modified as you wish.
To add new attachment, press `New attachment` and fill in the appeared input
To add a test step, press `New step` and fill in all fields
It is impossible to delete steps & attachments, so be careful. If you feel that the data provided is wrong, just press on the corresponding list item to the left and form contents will be set to default again
6. After all manipulations press `Generate Docx` button. This will create a `{req_id}.docx` file in the `{PROJECT}/reports` folder containing all needed info 