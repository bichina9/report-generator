var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var path = require('path');
var genReport = require('./generate-report');
app.use(bodyParser.json())

app.post('/generate-report', function (req, res) {
    console.log("req " , req);
    genReport(req.body, `${req.body.req_id}.docx`);
    res.send({status: "ok"});
});

app.get('/definitions', function(req,res) {
    res.sendFile(path.resolve(__dirname, 'output.json'));
});

app.listen(3001, function () {
    console.log('Example app listening on port 3000!');
});