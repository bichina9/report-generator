import React, {Component} from 'react';
import _ from 'lodash'
import axios from 'axios'

class TestScenario extends Component {

    constructor(props) {
        super(props);
        this.state = _.merge({
            req_id: '',
            data_setup_items: '',
            requirements: '',
            test_instructions: '',
            steps: [],
        }, props.scenario);
    }

    toggleInput = (e) => {
        this.setState({[e.target.name]: e.target.value})
    };

    toggleStepsChange = (ind, name, value) => {
        let steps = this.state.steps;
        steps[ind][name] = value;
        this.setState({steps: steps})
    };

    addNewStep = (e) => {
        e.preventDefault();
        this.setState({
            steps: [...this.state.steps, {
                POF: false,
                expected: '',
                actual: '',
                detailed: ''
            }]
        })
    };

    componentWillReceiveProps = (nextProps) => {
        this.setState(nextProps.scenario)
    };

    renderTestSteps = () => {
        return (
          <div>
              {_.map(this.state.steps, (step, ind) => <div key={ind}>
                  <div>----------------------STEP {ind + 1}------------------------</div>
                  <label>Detailed</label>
                  <input type="text"
                         className="form-control"
                         placeholder="Detailed test step ..."
                         value={this.state.steps[ind].detailed}
                  onChange={(e) => this.toggleStepsChange(ind, 'detailed', e.target.value)}/>
                  <label>Expected Results</label>
                  <textarea value={this.state.steps[ind].expected}
                            className="form-control"
                            placeholder="Expected results ..."
                            style={{"height" : "200px"}}
                            onChange={(e) => this.toggleStepsChange(ind, 'expected', e.target.value)}/>
                  <br/>
                  <label>Actual Results</label>
                  <br/>
                  <label>MATCH</label>
                  <input type="checkbox"
                         checked={this.state.steps[ind].match}
                         onChange={(e) => this.toggleStepsChange(ind, 'match', !this.state.steps[ind].match)}/>
                  {this.state.steps[ind].match
                    ? null
                  : <textarea value={this.state.steps[ind].actual}
                              placeholder="Actual Results, if not match ..."
                              className="form-control"
                              style={{"height" : "200px"}}
                              onChange={(e) => this.toggleStepsChange(ind, 'actual', e.target.value)}/>}
                  <br/>
                  <label>Passed</label>
                  <input type="checkbox"
                         checked={this.state.steps[ind].POF}
                         onChange={(e) => this.toggleStepsChange(ind, 'POF', !this.state.steps[ind].POF)}/>
                  <br/>
                  <label>References</label>
                  <textarea value={this.state.steps[ind].references}
                            placeholder="References ..."
                            className="form-control"
                            onChange={(e) => this.toggleStepsChange(ind, 'references', e.target.value)}/>
              </div>)}
              <button
                className="btn btn-default"
                onClick={this.addNewStep}>New step</button>
              <br/>
          </div>
        )
    };

    handleSubmit = (e) => {
        e.preventDefault();
        axios.post('/generate-report', this.state)
    };

    render() {
        return (
          <form>
              <div className="row">
                  <div className="col-md-2"></div>
                  <div className="col-md-8">
                      <label>Req ID</label>
                      <input name="req_id"
                             className="form-control"
                             type="text"
                             value={this.state.req_id}
                             onChange={this.toggleInput}/>
                      <br/>
                      <label>Data Setup / Attachments</label>
                      <textarea value={this.state.data_setup_items}
                                className="form-control"
                                placeholder="Data setup items / attachments ..."
                                name="data_setup_items"
                                onChange={this.toggleInput} />
                      <br/>
                      <label>Requirements</label>
                      <textarea value={this.state.requirements}
                                style={{"height" : "300px"}}
                                className="form-control"
                                name="requirements"
                                onChange={this.toggleInput} />
                      <br/>
                      <label>Test instructions</label>
                      <textarea value={this.state.test_instructions}
                                className="form-control"
                                style={{"height" : "200px"}}
                                name="test_instructions"
                                onChange={this.toggleInput} />
                      {this.renderTestSteps()}
                      <button
                        className="btn btn-default"
                        onClick={this.handleSubmit}>Generate Docx</button>
                  </div>
                  <div className="col-md-2"></div>
              </div>

          </form>
        );
    }
}

export default TestScenario;