import React, {Component} from 'react';
import {SideNav, Nav} from 'react-sidenav';
import _ from 'lodash'
import axios from 'axios'
import TestScenario from './TestScenario'

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            scenarios: [],
            selScenario: null
        }
    }

    componentDidMount = () => {
        axios.get('/definitions').then(res => {
            this.setState({scenarios: res.data})
        })
    };

    selectScenario = (ind) => {
        console.log("SET SCEN")
        this.setState({selScenario: _.clone(this.state.scenarios[ind])})
    };

    render = () => {
        console.log(this.state.selScenario)
        return (<div className="row">
              <div className="col-md-3">
                  <SideNav defaultSelectedPath="1">
                      {_.map(this.state.scenarios, (scenario, ind) => {
                          return (
                            <Nav
                              key={ind}
                              onClick={(e) => this.selectScenario(e.id)}
                              id={ind}>{scenario.req_id}</Nav>
                          )
                      })}
                  </SideNav>
              </div>
              <div className="col-md-9">
                  {this.state.selScenario === null ? null : <TestScenario scenario={this.state.selScenario}/>}
              </div>
          </div>
        )

    }

}

export default App;