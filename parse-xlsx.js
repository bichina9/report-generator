var fs = require('fs');
var _ = require('lodash')

var xlsx = require('node-xlsx').default;

const urd = xlsx.parse(fs.readFileSync(`${__dirname}/sheet.xlsx`));

const genDocumentDefinition = function(row) {
    return {
        req_id: row[0],
        requirements: row[3],
        test_instructions: row[5],
        steps: [],
        POF: row[6],
        author_initials: row[7]
    }
};

const result = _.map(urd[0].data, genDocumentDefinition)
fs.writeFileSync('./output.json', JSON.stringify(result), 'utf-8')