var JSZip = require('jszip');
var moment = require('moment')
var Docxtemplater = require('docxtemplater');
var fs = require('fs');
var path = require('path');
var _ = require('lodash')


function genReport(testData, outputFilename) {
    const defaultParameters = {
        effective_date: moment().format("MM/DD/YYYY"),
        date: moment().format("MM/DD/YYYY"),

        author_fullname: "Vladimir Pavlyuk",
        author_initials: "VP"

    };
    var tester_eval_passed = _.every(testData.steps, ['POF', true]);
    var attachments = _.map(testData.data_setup_items.split(/\r?\n/), function(name, ind) {
        return {head: "Attachment " + String.fromCharCode(ind + 65), name: name}
    });
    var content = fs.readFileSync(path.resolve(__dirname, 'report-template.docx'), 'binary');
    var zip = new JSZip(content);
    var doc = new Docxtemplater();
    doc.setOptions({linebreaks: true})
    doc.loadZip(zip);
    doc.setData(_.assign({}, defaultParameters, testData, {tester_eval_passed: tester_eval_passed, attachments: attachments}));
    try {doc.render()}
    catch (error) {
        const e = {
            message: error.message,
            name: error.name,
            stack: error.stack,
            properties: error.properties,
        };
        console.log(JSON.stringify({error: e}));
        throw error;
    }
    let buf = doc.getZip().generate({type: 'nodebuffer'});
    fs.writeFileSync(path.resolve(__dirname, `reports/${outputFilename}`), buf);
}

module.exports = genReport;